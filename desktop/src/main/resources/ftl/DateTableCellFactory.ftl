package ${projectPackage}.cell;

import javafx.beans.NamedArg;
import javafx.scene.Parent;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import java.text.SimpleDateFormat;
import java.util.function.Function;

/**
 * Created by ldh on 2018/3/27.
 */
public class DateTableCellFactory<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {

    private final String dateFormat;

    public DateTableCellFactory(@NamedArg("dateFormat") String dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    public TableCell<S, T> call(TableColumn<S, T> param) {
        return new TableCell<S, T>() {
            @Override
            protected void updateItem(T item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty || item.equals("")) {
                    setText(null);
                    setGraphic(null);
                } else {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
                    String str = simpleDateFormat.format(item);
                    setText(str);
                    setGraphic(null);
                }
            }
        };
    }
}
