package ${projectPackage}.cell;

import javafx.scene.Parent;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import java.util.function.Function;

/**
 * Created by ldh on 2018/3/27.
 */
public class ObjectTableCellFactory<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {

    private Function<T, Object> function;

    public ObjectTableCellFactory(Function<T, Object> function) {
        this.function = function;
    }

    @Override
    public TableCell<S, T> call(TableColumn<S, T> param) {
        return new TableCell<S, T>() {
            @Override
            protected void updateItem(T item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty || item.equals("")) {
                    setText(null);
                    setGraphic(null);
                    setStyle("");
                } else {
                    if (function != null) {
                        Object object = function.apply(item);
                        if (object != null) {
                            if (object instanceof Parent) {
                                setGraphic((Parent)object);
                                setText("");
                            } else {
                                setText(object.toString());
                            }
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                }
            }
        };
    }
}
