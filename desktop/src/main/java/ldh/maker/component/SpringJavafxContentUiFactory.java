package ldh.maker.component;

public class SpringJavafxContentUiFactory extends ContentUiFactory {

    public ContentUi create() {
        return new SpringJavafxContentUi();
    }
}
