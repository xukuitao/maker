package ${projectRootPackage};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
* Created by ldh on 2016/10/22.
*/
@SpringBootApplication
@ComponentScan(basePackages={"${projectRootPackage}"})
public class ApplicationBoot extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationaClass);
    }

    private static Class<ApplicationBoot> applicationaClass = ApplicationBoot.class;

    public static void main(String[] args) {
        SpringApplication.run(ApplicationBoot.class, args);
    }
}
