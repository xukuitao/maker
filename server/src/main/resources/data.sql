create TABLE tree_node(
  id int PRIMARY key auto_increment,
  name VARCHAR,
  text VARCHAR,
  parent_id int,
  type VARCHAR
);

create TABLE db_info(
  id int PRIMARY key auto_increment,
  name VARCHAR,
  ip VARCHAR ,
  port int,
  user_name VARCHAR ,
  password VARCHAR ,
  tree_node_id int
);

create TABLE setting(
  id int PRIMARY key auto_increment,
  xml VARCHAR,
  pojo VARCHAR ,
  dao VARCHAR,
  service VARCHAR ,
  controller VARCHAR ,
  db_name VARCHAR,
  enum_pack VARCHAR,
  service_interface BOOLEAN,
  json VARCHAR,
  tree_node_id int
);
create UNIQUE index db_name_tree_node_id_uidx on setting(tree_node_id, db_name);

create TABLE pojo(
  id int PRIMARY key auto_increment,
  table_name VARCHAR,
  name VARCHAR ,
  text VARCHAR,
  db_name VARCHAR,
  tree_node_id int
);
create index tree_node_id_db_table_idx on pojo(tree_node_id, db_name, table_name);

create TABLE pojo_filed(
  id int PRIMARY key auto_increment,
  column_name VARCHAR,
  name VARCHAR ,
  text VARCHAR,
  java_type VARCHAR,
  pojo_id int,
  tree_node_id int
);
create index pojo_id_idx_pf on pojo_filed(pojo_id);
create index tree_node_id_idx_pf on pojo_filed(tree_node_id);

create TABLE pojo_enum(
  id int PRIMARY key auto_increment,
  name VARCHAR,
  type VARCHAR,
  pack VARCHAR,
  text VARCHAR,
  column_name VARCHAR,
  tableName VARCHAR,
  pojo_id int,
  tree_node_id int
);
create index tableName_idx_pe on pojo_enum(tableName);
create index tree_node_id_idx_pe on pojo_enum(tree_node_id);

create TABLE pojo_function(
  id int PRIMARY key auto_increment,
  name VARCHAR ,
  is_create BOOL,
  pojo_id int,
  tree_node_id int
);
create index pojo_id_idx_pf2 on pojo_function(pojo_id);
create index tree_node_id_idx_pf2 on pojo_function(tree_node_id);

create TABLE javafx_setting(
  id int PRIMARY key auto_increment,
  pojo VARCHAR ,
  db_name VARCHAR,
  tree_node_id int
);
create index javafx_setting_idx_js on javafx_setting(tree_node_id);

insert into tree_node(id, name, text, parent_id, type) values(1, 'root', 'root', 0, 'ROOT');

create TABLE table_view(
  id int PRIMARY key auto_increment,
  db_name VARCHAR,
  table_name VARCHAR,
  alias VARCHAR,
  columns VARCHAR,
  tree_node_id int
);
create index table_view_idx_tid on table_view(tree_node_id);
create index table_view_idx_dbName on table_view(db_name);

create TABLE table_no(
  id int PRIMARY key auto_increment,
  db_name VARCHAR,
  content VARCHAR,
  tree_node_id int
);
create index table_no_idx_tid on table_no(tree_node_id);
create index table_no_idx_dbName on table_no(db_name);