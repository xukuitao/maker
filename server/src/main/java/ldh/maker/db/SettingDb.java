package ldh.maker.db;

import ldh.maker.util.UiUtil;
import ldh.maker.vo.DBConnectionData;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;
import org.apache.commons.dbutils.QueryRunner;

import java.sql.*;

/**
 * Created by ldh on 2017/3/26.
 */
public class SettingDb {

    public static SettingData loadData(TreeNode treeNode, String dbName) throws SQLException {
        SettingData data = null;
        Connection connection = UiUtil.H2CONN;
        String sql = "select * from setting where tree_node_id = ? and db_name = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, treeNode.getId());
        statement.setString(2, dbName);
        ResultSet rs = statement.executeQuery();
        if(rs.next()){
            data = new SettingData();
            data.setXmlPackageProperty(rs.getString("xml"));
            data.setPojoPackageProperty(rs.getString("pojo"));
            data.setDaoPackageProperty(rs.getString("dao"));
            data.setServicePackageProperty(rs.getString("service"));
            data.setControllerPackageProperty(rs.getString("controller"));
            data.setDbName(rs.getString("db_name"));
            data.setTreeNodeId(rs.getInt("tree_node_id"));
            data.setServiceInterface(rs.getBoolean("service_interface"));
            data.setJson(rs.getString("json"));
            data.setId(rs.getInt("id"));
        }
        statement.close();
        return data;
    }

    public static void save(SettingData data, TreeNode treeNode) throws SQLException {
        Connection connection = UiUtil.H2CONN;
        String sql = "insert into setting(xml, pojo, dao, service, controller, db_name, tree_node_id, service_interface, json) values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, data.getXmlPackageProperty());
        statement.setString(2, data.getPojoPackageProperty());
        statement.setString(3, data.getDaoPackageProperty());
        statement.setString(4, data.getServicePackageProperty());
        statement.setString(5, data.getControllerPackageProperty());
        statement.setString(6, data.getDbName());
        statement.setInt(7, treeNode.getId());
        statement.setBoolean(8, data.getServiceInterface());
        statement.setString(9, data.getJson());
        statement.executeUpdate();
        statement.close();
    }

    public static void update(SettingData data, TreeNode treeNode) throws SQLException {
        Connection connection = UiUtil.H2CONN;
        String sql = "update setting set xml = ?, pojo=?, dao=?, service=?, controller=?, service_interface=?, json=? where tree_node_id=? and db_name = ?";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, data.getXmlPackageProperty());
        statement.setString(2, data.getPojoPackageProperty());
        statement.setString(3, data.getDaoPackageProperty());
        statement.setString(4, data.getServicePackageProperty());
        statement.setString(5, data.getControllerPackageProperty());
        statement.setBoolean(6, data.getServiceInterface());
        statement.setString(7, data.getJson());
        statement.setInt(8, treeNode.getId());
        statement.setString(9, data.getDbName());
        statement.executeUpdate();
        statement.close();
    }

    public static void delete(TreeNode treeNode) throws SQLException {
        Connection connection = UiUtil.H2CONN;
        QueryRunner queryRunner = new QueryRunner();
        String sql = "delete from setting where tree_node_id = ?";
        queryRunner.update(connection, sql, treeNode.getId());
    }
}
