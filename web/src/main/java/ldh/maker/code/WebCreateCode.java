package ldh.maker.code;

import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.freemaker.*;
import ldh.maker.util.DbInfoFactory;
import ldh.maker.util.FreeMakerUtil;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

import java.util.*;

import static java.lang.System.arraycopy;

/**
 * Created by ldh on 2017/4/6.
 */
public class WebCreateCode extends CreateCode {

    protected List<String> jsFtls = new ArrayList<String>();
    protected Map<String, String> jspFtls = new HashMap<>();
    private volatile boolean isCreateMainPage = false;
    protected Map<String, String> otherData = new HashMap<>();

    public WebCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        super(data, treeItem, dbName, table);
    }

    public void addData(String key, String ftl) {
        otherData.put(key, ftl);
    }

    @Override
    protected void createOther(){
        if (table.isMiddle()) return;
        String controllerPath = createPath(data.getControllerPackageProperty());
        String beanName = FreeMakerUtil.firstUpper(table.getJavaName());
        String jsPath = createJsPath();
        WebControllerMaker controllerMaker = new WebControllerMaker()
                .pack(data.getControllerPackageProperty())
                .outPath(controllerPath)
                .className(beanName + "Controller")
                .beanMaker(pojoMaker)
                .beanWhereMaker(pojoWhereMaker)
//                .service(serviceInterfaceMaker)
                .imports(pojoWhereMaker)
                .ftl(otherData.get("controllerFtl"))
                .table(table)
                ;
        if (data.getServiceInterface()) {
            controllerMaker.service(serviceInterfaceMaker);
        } else {
            controllerMaker.service(serviceMaker);
        }
        controllerMaker.make();

        new MainControllerMaker()
                .controllerPackage(data.getControllerPackageProperty())
                .outPath(controllerPath)
                .make();

        if (jspFtls != null) {
            for (Map.Entry<String, String> ftl : jspFtls.entrySet()) {
                String db = treeItem.getValue().getData().toString();
                TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + db);
                String t = FreeMakerUtil.firstLower(table.getJavaName());
                String fileName = t.toLowerCase() + ftl.getValue() + ".jsp";
                String jspPath = createJspPath("jsp");
                new JspMaker()
                        .tableInfo(tableInfo)
                        .table(table)
                        .outPath(jspPath)
                        .ftl(ftl.getKey())
                        .fileName(fileName)
                        .make();
            }
        }

        if (jsFtls != null) {
            for (String jsFtl : jsFtls) {
                new JsMaker()
                        .table(table)
                        .outPath(jsPath)
                        .ftl(jsFtl)
                        .make();
            }
        }
        if (resourceMap != null) {
            if (isCreateMainPage) return;
            for (Map.Entry<String, String[]> entry : resourceMap.entrySet()) {
                String[] dirs = new String[entry.getValue().length-1];
                arraycopy(entry.getValue(), 1, dirs, 0, dirs.length);
                String path = createJspPath(dirs);
                String db = treeItem.getValue().getData().toString();
                TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + db);
                new JspMainMaker()
                        .tableInfo(tableInfo)
                        .ftl(entry.getKey())
                        .outPath(path)
                        .fileName(entry.getValue()[0])
                        .make();
            }
            isCreateMainPage = true;
        }
    }

    protected synchronized void mainPage(String jspPath) {
        if (isCreateMainPage) return;
        String db = treeItem.getValue().getData().toString();
        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + db);
        new JspMainMaker()
                .tableInfo(tableInfo)
                .outPath(jspPath)
                .make();
        isCreateMainPage = true;
    }

    public String getProjectName() {
        return "web";
    }
}
